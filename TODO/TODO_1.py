# -*- coding: utf-8 -*-
"""TOD0_1.ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/1iAvMgD0wUEROVu7Vvq0dCvgZFayBKKU5
"""

import requests
from bs4 import BeautifulSoup

url = 'https://en.wikipedia.org/wiki/Billboard_Year-End_Hot_100_singles_of_1970'
response = requests.get(url)

soup = BeautifulSoup(response.text)

dictionary = {}

for data in soup.findAll('tr')[1:101]:
  #print(data)

  title = data.findAll('a')[0].string
  print('Title: '+title)

  artist = data.findAll('a')[1].string
  print('Artist: '+artist)

  dictionary[title] = [artist]

print(dictionary)

